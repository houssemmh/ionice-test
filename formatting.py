import re

Gb_to_kbyte = 10 ** 6
Mb_to_kbyte = 10 ** 3
b_to_kbyte = 10 ** -3

read_regex= r"^Read (\d+\.?\d*)([a-zA-Z]+)"
written_regex= r" Written (\d+\.?\d*)([a-zA-Z]+) "
throughput_regex= r"\((\d+\.?\d*)([a-zA-Z]+)/sec\)"
time_regex = r"total time:\s+(\d+\.?\d*)"

def getValueWithUnit(regex, data):
	if ( re.search(regex, data, re.M) ):
		match = re.search(regex, data, re.M)
		value = float(match.group(1))
		unit = match.group(2)
		if (unit == "Gb"):
			unit_convert = Gb_to_kbyte
		elif (unit == "Mb"):
			unit_convert = Mb_to_kbyte
		elif (unit == "Kb"):
			unit_convert = 1
		elif (unit == "b"):
			unit_convert = b_to_kbyte
		value_kbytes = value * unit_convert
		return value_kbytes
	return -1

def getValue(regex, data):
	if ( re.search(regex, data, re.M) ):
		match = re.search(regex, data, re.M)
		value = float(match.group(1))
		return value
	return -1

def format_output(output):
	read = getValueWithUnit(read_regex,output)
	written = getValueWithUnit(written_regex,output)
	throughput = getValueWithUnit(throughput_regex,output)
	time = getValue(time_regex,output)
	return (read,written,throughput,time)