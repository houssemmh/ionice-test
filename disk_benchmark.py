#!/usr/bin/python3

import formatting
import lttngControl
from workloads import *
from multiprocessing import Process
import time

block_layer_events = (
	"block_rq_insert",
	"block_rq_issue",
	"addons_elv_merge_requests",
	"block_rq_complete",
	"block_getrq",
	"lttng_statedump_block_device",
	"block_bio_backmerge",
	"block_bio_frontmerge"
)

other_events = (
	"irq_handler_entry",
	"irq_handler_exit",
	"lttng_statedump_end",
	"lttng_statedump_file_descriptor",
	"lttng_statedump_process_state",
	"lttng_statedump_start",
	"sched_migrate_task",
	"sched_process_exec",
	"sched_process_exit",
	"sched_process_fork",
	"sched_switch",
	"sched_wakeup",
	"sched_wakeup_new",
	"softirq_entry",
	"softirq_exit"
)

all_events = block_layer_events + other_events



def invert_priority(process):
	if process['priority'] == 0:
		set_highpriority(process['pid'])
		process['priority'] = 1
	elif process['priority'] == 1:
		set_lowpriority(process['pid'])
		process['priority'] = 0

if __name__=="__main__":
	
	lttngControl.start_lttng(all_events,1)
	
	#Processes creation
	process1 = sysbench_process1()
	p1 = {'pid': process1.pid +1, 'priority' : 0}
	
	time.sleep(0.1)

	process2 = sysbench_process2()
	p2 = {'pid': process2.pid +1, 'priority' : 1}
	
	print("Process 1: "+ str(p1['pid']))
	print("Process 2: "+ str(p2['pid']))

	for i in range(0,8):
		time.sleep(2)
		invert_priority(p1)
		invert_priority(p2)

	# Wait for processes
	process1.wait()
	process2.wait()
	lttngControl.stop_lttng()

