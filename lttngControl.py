import subprocess

external_output_directory = "/home/houssemmh/traces/"
internal_output_directory = "/mnt/hdd-ext2/traces/"

def execute(command):
    subprocess.check_call(command.split(" "))

def start_lttng(event_list,external):
	output_directory = external_output_directory if external else internal_output_directory
	execute("rm -rf my_trace")
	execute("lttng create -o {}/my_trace".format(output_directory))
	execute("lttng enable-channel -k --subbuf-size 16384 --num-subbuf 4096 my_channel")
	execute("lttng enable-event -k {} -c my_channel".format(",".join(event_list)))
	execute("lttng enable-event -k --syscall --all -c my_channel")
	execute("lttng add-context -k --type tid --type pid -c my_channel")
	execute("lttng start")

def stop_lttng():
	execute("lttng destroy -a")
