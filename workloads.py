import subprocess

lowpriority_directory = "/mnt/hdd-ext4/lowpriority"
highpriority_directory = "/mnt/hdd-ext4/highpriority"
mydir = "/home/houssemmh/bench"

def drop_caches():
        with open('/proc/sys/vm/drop_caches', 'w') as dropcache:
                dropcache.write('3\n')


def sysbench_process1():
	command = "ionice -c 2 -n 7 sysbench --test=fileio --file-total-size=500M --file-num=1 --file-test-mode=seqrd --file-block-size=2M --num-threads=1 run"
	process = subprocess.Popen(command.split(" "), cwd=lowpriority_directory, stdout=subprocess.PIPE)
	return process

def sysbench_process2():
	command = "ionice -c 1 -n 0 sysbench --test=fileio --file-total-size=500M --file-num=1 --file-test-mode=seqrd --file-block-size=2M --num-threads=1 run"
	process = subprocess.Popen(command.split(" "), cwd=highpriority_directory, stdout=subprocess.PIPE)
	return process

def set_lowpriority(pid):
	command = "ionice -c 2 -n 7 -p {}".format(pid)
	process = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE)
	process.wait()

def set_highpriority(pid):
	command = "ionice -c 1 -n 0 -p {}".format(pid)
	process = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE)
	process.wait()


def sysbench_sda():
        command = "ionice -c 1 -n 0 sysbench --test=fileio --file-total-size=10G --file-num=1 --file-test-mode=seqrd --file-block-size=2M --num-threads=1 run"
        process = subprocess.Popen(command.split(" "), cwd=mydir, stdout=subprocess.PIPE)
        return process
